# Manual CLI

Kumpulan perintah cli yang sering digunakan untuk beberapa framework.

1. [Firebase](firebase.md)
1. [Git](git.md)
1. [Heroku](heroku.md)
1. [Hugo](hugo.md)
1. [Netlify](netlify.md)
1. [Nextcloud](nextcloud.md)
1. [Now](now.md)
1. [Npm](npm.md)
1. [PowerShell](powershell.md)
1. [Vercel](vercel.md)
1. [Vue-cli](vue-cli.md)
1. [Other](other.md)

## Demo site

URL | Powered by
---------|----------
 https://git.erto.co/manual/ | [Github](https://git.erto.co/manual/)
 https://manual.smvi.co/ | [Small Victories](https://manual.smvi.co/)
